# Script I wrote to make Raspberry pies automatically play music #

Below are some install instructions:

* Format usb drives with the name “music” and then you can load any mp3 files onto it and it will play.
* Clone repository (git clone https://jamesla@bitbucket.org/mrautomate/mra_player.git) 
* Give install.sh execute permissions (sudo chmod +x install.sh)
* Install with (sudo ./install.sh)

Troubleshooting
* sudo systemctl status mra_player
